<?php

namespace App\Http\Controllers;


use App\Constants\DBMessage;
use Exception;


class AppController extends Controller
{

	public function fetchMessage()
	{
		try {

			return $this->jsonData(array(
				'requestFailedMessage' => DBMessage::API_SERVER_ERROR_MESSAGE
			));
		} catch (Exception $e) {
			return $this->jsonError($e, __CLASS__, 'fetchMessage');
		}
	}
}
