<?php


namespace App\Http\Controllers;


use Exception;
use App\Constants\DBCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function __construct()
    {

    }

    public function login(Request $req)
    {
        try {

            $this->customValidate($req->all(), array(
                'username:Nama pengguna' => 'required|string',
                'password:Kata sandi' => 'required|string',
            ));

            $credentials = $req->only(['username', 'password']);

            if (! $token = Auth::attempt($credentials)) {
                throw new Exception("Nama pengguna atau kata sandi tidak ditemukan", DBCode::AUTHORIZED_ERROR);
            }

            $response = \auth()->user();
            $response['token'] = $token;

            return $this->jsonSuccess(null, $response);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function me()
    {
        try {
            return $this->jsonSuccess(null, \auth()->user());
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }
}
