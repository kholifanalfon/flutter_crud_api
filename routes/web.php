<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function() use ($router) {

    $router->post('/login', ['uses' => 'AuthController@login']);
    $router->get('/me', ['uses' => 'AuthController@me']);
});

$router->group(['middleware' => 'auth'], function() use ($router) {

    $router->group(['namespace' => 'Masters'], function() use ($router) {
        $router->group(['prefix' => 'types'], function() use ($router) {

            $router->get('/select', ['uses' => 'TypesController@selectApi']);
            $router->post('/datatables', ['uses' => 'TypesController@datatables']);
            $router->options('/datatables', ['uses' => 'TypesController@datatables']);

            $router->post('/', ['uses' => 'TypesController@store']);
            $router->get('/{id}', ['uses' => 'TypesController@show']);
            $router->put('/{id}', ['uses' => 'TypesController@update']);
            $router->delete('/{id}', ['uses' => 'TypesController@destory']);
        });
    });
});
